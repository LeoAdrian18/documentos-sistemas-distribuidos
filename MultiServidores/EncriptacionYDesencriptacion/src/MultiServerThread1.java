import java.net.*;
import java.io.*;
import java.util.Base64;
import java.util.Date;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

public class MultiServerThread1 extends Thread {
   private Socket socket = null;
   private static String Temperatura;
   private static String Viento;
   private static String Humedad;
   private static String DolarCompra;
   private static String DolarVenta;
   private static String Digitos;
   private static final String pass = "123";
   private static final String clave = "123";
   
   public MultiServerThread1(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }
   private static String encriptar(String s) throws UnsupportedEncodingException{
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }
   private static String desencriptar(String s) throws UnsupportedEncodingException{
        byte[] decode = Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    }



   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
	 String separador = Pattern.quote("|");
         int tam;
         String NoService;
         String minusculas;
	 lineIn = entrada.readLine();
            
                System.out.println("Received: "+lineIn);
                escritor.flush();
                     //System.out.println(lineIn.charAt(0));
                tam = lineIn.length();
                     //System.out.println(tam);
                if(((lineIn.charAt(0)) == '#') && ((lineIn.charAt(tam-1)) == '&')){
                    System.out.println("HAN SOLICITADO UN SERVICIO");
                    NoService = String.valueOf(lineIn.charAt(1))+String.valueOf(lineIn.charAt(2))+String.valueOf(lineIn.charAt(3));
                    //escritor.println("Usted solicito un servicio: "+NoService);
                    System.out.println(NoService);
                    escritor.flush();

                    if(NoService.equals("123")){
                        escritor.println("Servicio 123 del server2");
                        System.out.println("Servicio 123 del server2");
                }
                    else if (NoService.equals("CRY") || NoService.equals("DEC")){
                        String[] parts = lineIn.split(separador);
                        //System.out.println(parts.length);
                        //System.out.println(parts[0]);
                        //System.out.println(parts[1]);
                        //System.out.println(parts[2]);
                        escritor.flush();
                        if(parts.length == 3){
                            minusculas = parts[2];
                            minusculas = ServerMultiClient.delate(minusculas);
                            
                            String cadenaEncriptada = "";
                            try {
                                System.out.println("Cadena original > "+minusculas);
                                if(NoService.equals("CRY")){
                                    cadenaEncriptada = encriptar(minusculas);
                                    System.out.println("Cadena encriptada > "+cadenaEncriptada);
                                    escritor.println("#R-CRY|1|"+cadenaEncriptada+"&");
                                    escritor.flush();}
                                else if(NoService.equals("DEC")){
                                    System.out.println("Cadena original > "+minusculas);
                                    cadenaEncriptada = desencriptar(minusculas);
                                    System.out.println("Cadena encriptada > "+cadenaEncriptada);
                                    escritor.println("#R-DEC|1|"+cadenaEncriptada+"&");
                                    escritor.flush();
                                }
                            } catch (UnsupportedEncodingException uee) {
                                if(NoService.equals("CRY")){
                                    System.out.println("#R-CRY|1|error: "+uee+"&");
                                    escritor.println("#R-CRY|1|error: "+uee+"&");
                                    escritor.flush();}
                                else if(NoService.equals("DEC")){
                                    System.out.println("#R-DEC|1|error: "+uee+"&");
                                    escritor.println("#R-DEC|1|error: "+uee+"&");
                                    escritor.flush();}
                            }
                            //escritor.println(minusculas.toLowerCase());
                            escritor.flush();
                        }
                        
                        else{

                            escritor.println("Error en el protocolo [Delimitadores]");
                            escritor.flush();
                        }
                        
                    }
                    else{
                        escritor.println("Servicio Desconocido");
                        escritor.flush();
                    }   
                }
                else{
                    if(lineIn.equals("FIN")){
                   ServerMultiClient.NoClients--;
                                  
                               }else{
                   escritor.println("Echo... "+lineIn);
                   escritor.flush();
                }
                }
         try{		
            entrada.close();
            escritor.close();
            socket.close();
             System.out.println("Cerrando la conexion");
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
