import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.regex.Pattern;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
	 String separador = Pattern.quote("|");
         int tam;
         String NoService;
         String minusculas;
	     while((lineIn = entrada.readLine()) != null){
            
                System.out.println("Received: "+lineIn);
                escritor.flush();
                     //System.out.println(lineIn.charAt(0));
                tam = lineIn.length();
                     //System.out.println(tam);
                if(((lineIn.charAt(0)) == '#') && ((lineIn.charAt(tam-1)) == '&')){
                    System.out.println("HAN SOLICITADO UN SERVICIO");
                    NoService = String.valueOf(lineIn.charAt(1))+String.valueOf(lineIn.charAt(2))+String.valueOf(lineIn.charAt(3));
                    escritor.println("Usted solicito un servicio: "+NoService);
                    escritor.flush();

                    if(NoService.equals("018")){
                        String[] parts = lineIn.split(separador);
                        //System.out.println(parts.length);
                        //System.out.println(parts[0]);
                       System.out.println(parts[1]);
                        //System.out.println(parts[2]);
                        escritor.flush();
                        if((parts.length == 3) && (parts[1].equals("1"))){
                            minusculas = parts[2];
                            minusculas = ServerMultiClient.delate(minusculas);
                            escritor.println("#R-018|1|"+minusculas.toLowerCase()+"&");
                            escritor.flush();
                        }
                        else{

                            escritor.println("#R-018|1|Fallo en el protocolo"+"&");
                            escritor.flush();
                        }
                }
                    else{
                        escritor.println("Servicio Desconocido");
                        escritor.flush();
                    }
                }
                else{
                    if(lineIn.equals("FIN")){
                   ServerMultiClient.NoClients--;
                                  break;
                               }else{
                   escritor.println("Echo... "+lineIn);
                   escritor.flush();
                }
                }
              
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
} 
